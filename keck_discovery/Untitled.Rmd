---
title: "Keck Medicine Web Strategy Report"
author: "Modea Analysts"
date: "`r format(Sys.Date(), '%B %d, %Y')`"
output:
  modeafun::modea_pdf: default
  modeafun::modea_html: default
# knit: pagedown::chrome_print
---  

```{r setup, include=FALSE}
# Add commented out options if warnings are unimportant
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)
```

```{r attach-packages, echo=FALSE}
library(tidyverse)
library(modeafun)
library(ggthemes)
library(modeasecure)
library(scales)
library(ggrepel)
library(glue)
library(lubridate)
library(kableExtra)
library(wordcloud)
library(googleAnalyticsR)
library(googleAuthR)
# write your classifier function
classifer_keck <- function(path, host){
    case_when(
      str_detect(path, "(not set)") ~ "exit",
      path == "(entrance)" ~ "entrance",
      str_detect(path, "/locations|parking|location|center|clinic|-st$|-street$") ~ "locations",
      str_detect(host, "myuscchart") ~ "patient portal",
      str_detect(path, "physician|/doctor|provider") ~ "provider profiles",
      str_detect(path, "meet-|-team|find-a") ~ "find-a-doc / doc list pages",
      str_detect(path, "treatments|condition|services") ~ "specialties & services",
      str_detect(path, "\\.org\\?") ~ "global search",
      !str_detect(host, "www|providers") ~ "specialties & services",
      path == "/" ~ "home",
      str_detect(path, "coronavirus/|/telehealth") ~ "telemed / corona",
      str_detect(path, "can-|is-|are-|will-|-[0-9]-|do-|how-|what-|whats-") | str_count(path, pattern = "-") > 4 ~ "blog",
      TRUE ~ "other"
    )
}

```


```{r data pulls}
date_range <- c("2019-12-01", "2020-06-01") #select date range, 1st 6 months of 2020
GA_pull_discovery <- # set initial parameters
  modeasecure::GA_pull_factory(
  email = "analytics.team@modea.com",
  view_id = 75903716,
  dates = date_range
)

demographics <- 
  GA_pull_discovery(
    metrics = 
      c(
        "users"
        ),
    dimensions = 
      c(
        "userAgeBracket",
        "userGender",
        "deviceCategory"
        ),
  )
ref_filt <- filter_clause_ga4( list(dim_filter("fullReferrer", "REGEXP", "keckmedicine", not = TRUE)))
top_landings <- 
  GA_pull_discovery(
    metrics = c(
      "sessions",
      "bounces"
    ),
    dimensions = c(
      "landingPagePath",
      "hostname"
    ),
    dim_filters = ref_filt
  ) %>% 
  mutate(
    type = classifer_keck(landing_page_path, hostname)
  )
patterns <- 
  GA_pull_discovery(
    metrics = 
      c(
        "sessions"
      ),
    dimensions = 
      c(
        "landingPagePath",
        "secondPagePath",
        "hostname"
      ),
    dim_filters = ref_filt
  ) %>% 
  mutate(
    type_1= classifer_keck(landing_page_path, hostname),
    type_2 = classifer_keck(second_page_path, hostname)
  )
flow <- 
  GA_pull_discovery(
    metrics = 
      c(
        "pageviews"
      ),
    dimensions = 
      c(
        "pagePath",
        "previousPagePath",
        "hostname"
      ),
    dim_filters = ref_filt
  ) %>% 
  mutate(
    type_1= classifer_keck(page_path, hostname),
    type_2 = classifer_keck(previous_page_path, hostname)
  )

```



# Purpose and Goals

Our purpose is to provide Keck Medicine and the Internal Modea team with additional support around decision-making for redesign and development of the keckmedicine.org site.

# Key questions

Our primary areas of inquiry are pretty simple: Who is using this site? What are the 'front doors" of the site, or where users are entering the site? How do people move around the site?

And, above all, what can all this tell us about the UX of the site as it stands, and what users might want or expect that to be?

# Who uses the site?

```{r demographics}
demographics %>% 
  ggplot(
    aes(
      x = user_age_bracket,
      y = users,
      fill = user_gender
    )
  )+
  geom_col(position="dodge")+
  scale_fill_few()+
  scale_y_continuous(labels=comma)+
  theme_fivethirtyeight()+
  labs(fill="")+
  theme(axis.text.y = element_blank())+
  ggtitle("Who uses Keckmedicine.org?")
```

The short answer to this question is still "more younger men,  more older women," but there are two ways that Keck Medicine's audience differs from other clients in the healthcare space:

First, the gender gap is slightly less pronounced than in other, more generalist regional healthcare providers. 

Second, the audience skews older -- especially on the female side. Given that Keck is a specialty hospital, that stands to reason as older patients base are more likely to need specialty care.

# How do these individuals use the site?

```{r devicetype}
demographics %>% 
  ggplot(
    aes(
      x = user_age_bracket,
      y = users,
      fill = device_category
    )
  )+
  geom_col(position="stack")+
  facet_wrap(.~user_gender)+
  scale_fill_few()+
  scale_y_continuous(labels=comma)+
  theme_fivethirtyeight()+
  ggtitle(
    "What devices do people use on KeckMedicine.org"
  )+
  labs(fill="")
```

In short, the site usage is overwhelmingly mobile, and mobile at every age and gender split. 

It's also worth noting that tablet usage increases for older users, especially women. 
# Where do users enter the site?

```{r}
# plot
top_landings %>% 
  group_by(type) %>% 
  summarise(t = sum(sessions), bounces = sum(bounces)) %>% 
  ggplot(
    aes(
      x = reorder(type, -t),
      y = t,
    )
  )+
  geom_col()+
  labs(x = "", y = "sessions")+
  scale_y_continuous(labels =comma)+
  theme_fivethirtyeight()+
  scale_fill_few()+
  theme(axis.text.x = element_text(angle=40, hjust=1))+
  ggtitle("Where people enter KeckMedicine.org")
```

Unsurprisingly, users mostly enter the site via the blog! However, most people who don't enter on the blog, enter the site on one of the many specialty sub-domains. These specialty subdomains are also extremely important to the functioning of the site, as are the provider profile pages.

One thing of note here is that location pages tend to be less-used as an entrance page, implying these pages are more interior in design. Given Location pages tend to perform well in SEO rankings, there may be an opportunity to more aggressively market those location pages. 

However, it is worth noting that blog posts have an exceptionally high exit rate, meaning users don't always transition into the rest of the site at a high rate. 

```{r bounce share}
palette_test <- c("goldenrod", "royalblue")
patterns %>% 
  mutate(
    exited = if_else(type_2 == "exit", T, F)
  ) %>% 
  group_by(
    type_1,
    exited
  ) %>% 
  summarise(sessions = sum(sessions)) %>% 
  filter(type_1 != "exit") %>% 
  ggplot(
    aes(
      x = reorder(type_1, -sessions),
      y = sessions,
      fill = exited
    )
  )+
  geom_col()+
  labs(x = "", y = "sessions")+
  scale_y_continuous(labels =comma)+
  scale_fill_few()+
  theme_fivethirtyeight()+
  theme(axis.text.x = element_text(angle=40, hjust=1))+
  ggtitle("Where people enter KeckMedicine.org - and don't exit")
```

So, we can see here that the blog especially, regardless of how much traffic and awareness it brings in, isn't sending most of its traffic on to the rest of the site.

Importantly: this may be totally fine! The downstream effects of blog traffic are important and should not be discounted. A user can come back in a later session and convert, and even if they don't, the brand awareness of those blog posts is also extremely important. It's just to say that there's a large cohort of users who 'enter' the site on the blog, and don't then browse further, and that percentage is much lower than other areas of the site.

# Connectivity

What we're looking at here is the % of sessions that not only stick on the site, but navigate to a separate area of the site.

We want users to be able to quickly and easily navigate across various areas of content on the site. If they are searching for additional information, it should be readily available to them, without having to move through too many layers of extra clicking.

Here, we see that the homepage and location pages are the most likely to send users to another area.

```{r connect}
flow %>% 
  filter(type_2 != "entrance" ) %>% #& type_2 !="exit") %>% 
  group_by(
    type_1,
    type_2
  ) %>% 
  summarise(
    total = sum(pageviews)
  ) %>% 
  group_by(type_2) %>% mutate(group_total = sum(total), per = total / group_total) %>%
  mutate(
    color = 
    case_when(
      type_1 == type_2 ~ "same",
      TRUE ~ type_1
    )
  ) %>% 
  filter(type_2 != "patient portal") %>% 
  mutate(
    color = relevel(as.factor(color), "same")
  ) %>% 
  ggplot(
    aes(
      x = reorder(type_2, group_total),
      y = per,
      fill = color
    )
  )+
  geom_col(alpha = 0.9)+
  theme_fivethirtyeight()+
  scale_fill_stata()+
  coord_flip()+
  labs(fill="")+
  ggtitle("Where do people navigate to from top areas?")

```

Notice here that specialties and service content tends to send users within the specialties and services content -- this makes sense, as that content encompasses quite a few different areas. Many of the specialty subdomains have quite a bit of content to them beyond just linking to other areas. However, since these pages are quite popular, it's worth thinking about how to better connect those specialties with other areas of the site (location content and provider content, specifically) It is also worth noting that specialty content tends to require multiple clicks to get to another area of the site. There is possiblly a great opportunity for increasing that connective tissue across the subdomains.

As a contrast, look at the homepage, which sends users to a variety of different places on the site, as the homepage is supposed to do. Locations and provider profiles, respectively, tend to offer more connective tissue as well with other areas of the site. 

Also of note: even those users who navigate beyond the initial blog post, tend not to venture outside of the blog content itself at a high rate.

# Takeaways

### Mobile usage should be prioritized: 

Given the site's hard skew towards mobile and tablet users, mobile device usability is critical going forward. That being said, Keck Medicine's user base is largely older, meaning not digitally native, so ease of use and clarity of navigation also take precedence.

### The blog brings in the volume... 

The blog posts are excellent, and bring in millions of sessions each year. It's a powerful engine for SEO performance, brand awareness, and building trust with the Keck Medicine site.

### ... but specialties and providers bring in users

These types of content do the lion's share of acquiring new users for the site that actually stick around. While the blog brings in great volume, for truly bringing users into the wider site, Provider Profiles and Specialty content do most of the work. It's worth thinking about how those pages could better connect to other areas of the site.

### Specialty Content doesn't readily connect with other areas of the site

Specialty content tends to keep users in the relative silo of content. That may be desirable, but we also want users to be able to find the information they need easily, and more firm and easy linkages between those three key areas (specialties, providers and locations), is also key.



